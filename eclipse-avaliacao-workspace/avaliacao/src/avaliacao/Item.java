package avaliacao;

public class Item {
	private String title;
	private String publisher;
	private String yearPublisher;
	private String ISBN;
	private String price;
	
	public Item(String title, String publisher, String yearPublisher, String ISBN, String price) {
		super();
		this.title = title;
		this.publisher = publisher;
		this.yearPublisher = yearPublisher;
		this.ISBN = ISBN;
		this.price = price;
	}

	public void display() {
		System.out.println("Titulo: "+this.title+"Editora: "+this.publisher+"Data: "+this.yearPublisher+"ISBN: "+this.ISBN+"Preço: "+this.price);
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getYearPublisher() {
		return yearPublisher;
	}
	public void setYearPublisher(String yearPublisher) {
		this.yearPublisher = yearPublisher;
	}
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}

}


