package avaliacao;

public class Book extends Item{
	private String volume;
	private String author;
	private String edition;
	
	public Book(String title, String publisher, String yearPublisher, String ISBN, String price, String author, String edition, String volume) {
		super(title, publisher, yearPublisher, ISBN, price);
		this.author=author;
		this.edition=edition;
		this.volume=volume;
	}
	
	public void display() {
		System.out.println("Titulo: "+getTitle()+"Editora: "+getPublisher()+"Data: "+getYearPublisher()+"ISBN: "+getISBN()+"Preço: "+getPrice()+"Autor: "+this.author+"Edição: "+this.edition+"Volume:"+this.volume);
	}
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}

}

